/**
 * jQuery plugin to show/hide parts of the form based on an expression defined as "data-if" attribute.
 * Expression is formed of the state(s) of any inputs of parent element (form).
 * @see parser.js anf compiler.js for details.
 *
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 */
(function ($) {
  'use strict';

  var parser = require('./parser')
    , compiler = require('./compiler');

  /**
   * Plugin class definition.
   *
   * @param {jQuery} $el
   * @constructor
   */
  var ShowMeIf = function ($el) {
    this.$el = $el;
    this.getEmitters();
    this.watch();
  };

  /**
   * Collect all input fields to listen their "change" event.
   */
  ShowMeIf.prototype.getEmitters = function () {
    var _this = this,
      selectors = _this.emitters = [];

    _this.listeners = _this.$el.find('[data-if]').each(function () {
      var $el = $(this),
        expr = $el.data('if'),
        parsed = parser.parse(expr);

      if (parsed) {
        // set listener's initial visibility
        _this.toggle($el);

        // collect selectors from parsed expression
        $.each(parsed, function (k, v) {
          selectors.indexOf(v.selector) === -1 && selectors.push(v.selector);
        });
      }
    });
  };

  /**
   * Listen for "change" event on emitters.
   *
   * @returns {boolean}
   */
  ShowMeIf.prototype.watch = function () {
    var _this = this,
      emitters = _this.emitters;

    if (!emitters.length) {
      return false;
    }

    // listen all possible emitters
    // cause coupled radio buttons fire only one change event pr namespace
    _this.$el.on('change', ':input:not(:button)', function () {
      _this.listeners.each(function () {
        _this.toggle(this);
      });
    });
  };

  /**
   * Show/hide a container and disable/enable it child input fields
   * based on evaluated expression if "data-if" attribute.
   *
   * @param {jQuery|HTMLElement} el
   */
  ShowMeIf.prototype.toggle = function (el) {
    var $el = $(el),
      show = compiler.compile($el.data('if'), this.$el);
    $el[show ? 'fadeIn' : 'hide']()
      .find(':input')
      .add($el) // if it's an input then handle it to
      .prop('disabled', !show);
  };

  /**
   * Check if jQuery version valid.
   * Plugin requires at least version 1.6 as we rely on prop()
   *
   * @returns {boolean}
   */
  var isValidLibVersion = function () {
    var ver = $.fn.jquery.split('.');
    return !(ver[0] === 0 || (ver[0] === 1 && ver[1] < 6));
  };

  /**
   * Plugin definition.
   *
   * @returns {jQuery}
   */
  $.fn.showMeIf = function () {

    if (!isValidLibVersion()) {
      return console.error('ShowMeIf: jQuery is too old. Need at least v1.6.');
    }

    var componentName = 'ShowMeIf';
    return this.each(function () {
      var $el = $(this),
        data = $el.data(componentName);
      !data && $el.data(componentName, new ShowMeIf($el));
    });
  };

  /**
   * Automatically initialize plugin on marked elements.
   */
  $(function () {
    $('.js-show-me-if').showMeIf();
  });
})(jQuery);



