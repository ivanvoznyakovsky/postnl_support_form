/**
 * Helper class for parsing string expression into an object representation.
 *
 * Example: <pre><code>.inputCheckbox || #inputText && (inputEmail:valid && inputSelect)</code></pre>
 * Each part of expression is a valid jQuery selector: <pre><code>.className/#inputId/inputName</code></pre>
 * If a part of expression has no trailing "." or "#" character then it's assumed to be an input name.
 * and will be interpreted as follows: <pre><code>:input[name='inputEmail']</code></pre>
 *
 * The following modifiers are supported: checked, selected, valid: <pre><code>inputEmail:valid</code></pre>
 * Modifiers can not be combined in this version.
 * Which means you can not do something like: <pre><code>inputEmail:valid:selected</code></pre>
 *
 * If no modifier given to a condition <pre><code>:checked, :selected, :valid</code></pre> then value of the input will be checked.
 *
 * Give example above will eventually be parsed as following set of conditions:
 * <pre><code>
 *     {
 *        '.inputCheckbox': {
 *          prop    : 'value',
 *          selector: '.inputCheckbox'
 *        },
 *        '#inputText': {
 *          prop    : 'value',
 *          selector: '#inputText'
 *        },
 *        'inputEmail:valid': {
 *          prop    : 'valid',
 *          selector: ':input[name="inputEmail"]'
 *        },
 *        'inputSelect': {
 *          prop    : 'value',
 *          selector: ':input[name="inputSelect"]'
 *        }
 *     }
 * </code></pre>
 *
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 */

'use strict';

var parser = (function () {

  require('./polyfill').string();

  /**
   * Parser class definition.
   *
   * @constructor
   */
  var Parser = function () {
    this.cache = {};
  };

  /**
   * Parse expression or get it from cache if exists.
   *
   * @param {string} expression
   * @returns {object|false}
   */
  Parser.prototype.parse = function (expression) {
    if (!expression) {
      console.warn('ShowMeIf parser: expression is empty.');
      return false;
    }

    var cache = this.cache;

    if (!cache[expression]) {
      // trim whitespaces
      expression = expression
        .trim()
        .replace(/\s+/g, ' ') // two or more spaces to one
        .replace(/\(|\)|!/g, ''); // remove brackets and negations

      var parts = expression.split(/&&|\|\|/).map(function (el) {
        return ('' + el).trim();
      });

      if (!parts.length) {
        console.warn('ShowMeIf parser: invalid expression "%s".', expression);
        return false;
      }

      var expressionFields = {},
        fieldsEmpty = true,
        i = parts.length,
        part,
        selector,
        matches;

      for (; i--;) {
        part = parts[i];
        matches = part.match(/^((?:\.|#)?(?:\[)?.+(?:\])?)/);

        if (!matches || !matches[1]) {
          continue;
        }

        selector = matches[1];
        matches = selector.match(/(.+):(\w+)?$/);

        if (matches) {
          selector = matches[1];
        }

        // if it's not a class/id selector
        // and not input name selector notation
        if (!/\.|#|\[|\]/g.test(selector)) {
          selector = ':input[name="' + selector + '"]'
        }

        var prop = (matches && matches[2]) || 'value';
        expressionFields[part] = {
          prop    : ('' + prop).toLowerCase().trim(),
          selector: selector.trim()
        };

        fieldsEmpty = false;
      }

      if (!fieldsEmpty) {
        cache[expression] = expressionFields;
      }
    }

    return cache[expression];
  };

  return new Parser();
})();

module.exports = parser;
