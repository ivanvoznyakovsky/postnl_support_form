/**
 * Helper class to compile conditions set into boolean result.
 * NEVER GROUP CONTAINERS UNDER A COMMON CONDITION
 * CAUSE IF CHILD CONTAINER'S CONDITION WILL EVALUATE TO "TRUE"
 * IT'LL MAKE ALL INTERNAL INPUTS "ENABLED" EVEN IF PARENT'S CONTAINER CONDITION WILL BE "FALSE"
 * Eg.:
 * <pre>
 *     <div data-if="commonCondition">
 *         <div data-if="child1Condition">
 *         <div data-if="child2Condition">
 *         <div data-if="child3Condition">
 *     </div>
 * </pre>
 *
 * See parser.js for input format.
 *
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 */

'use strict';

var compiler = (function ($) {

  var parser = require('./parser');

  /**
   * @type {boolean}
   */
  var supportsNativeValidation = (function () {
    return typeof document.createElement('input').checkValidity === 'function';
  })();

  /**
   * @type {boolean}
   */
  var isTestEnv = /PhantomJS/.test(window.navigator.userAgent);

  /**
   * Evaluates set of boolean condition against given scope
   *
   * @param {object} condition
   * @param {string} condition.prop
   * @param {string} condition.selector
   * @param {jQuery} $scope
   * @returns {boolean}
   */
  var evalCondition = function (condition, $scope) {
    if (!$scope || !$scope.length || $.isEmptyObject(condition)) {
      return false;
    }

    var $el = $scope.find(condition.selector).filter(':input:not(:button)');

    if (!$el.length) {
      return false;
    }

    var propToCheck = condition.prop,
      inputHasVal = !!$el.val();

    if (!inputHasVal) {
      return false;
    }

    var result = inputHasVal;
    if (propToCheck === 'value') {
      // if it's a radio button or a checkbox check it's state
      if (['radio', 'checkbox'].indexOf($el[0].type) >= 0) {
        result = inputHasVal && !$el.prop('disabled') && !!$el.filter(':checked').length;
      }
    } else if (propToCheck === 'valid') {
      // check for valid/invalid classes first
      if ($el.hasClass('valid') || $el.hasClass('invalid')) {
        result = !$el.hasClass('invalid');
      } else {
        // if we can not check input's validity natively then assume it's valid
        result = supportsNativeValidation ? $el[0].checkValidity() : true;
      }
    } else {
      result = $el.prop(propToCheck);
    }

    return result;
  };

  /**
   * Escape string for RegExp.
   *
   * @param {string} str
   * @returns {string}
   */
  var escape = function (str) {
    return ('' + str).replace(/(!|\.|\[|\])/g, '\\$1');
  };

  /**
   * Compiler class definition.
   *
   * @constructor
   */
  var Compiler = function () {
    this.reCache = {};
    this.inputsCache = {};
  };

  /**
   * Create a new RegExp object based on expression
   * or get it from cache if exists.
   *
   * @param {string} expression
   * @returns {RegExp}
   */
  Compiler.prototype.getRegExp = function (expression) {
    var cache = this.reCache;
    if (!cache[expression]) {
      cache[expression] = new RegExp(escape(expression + '\\b'), 'g');
    }

    return cache[expression];
  };

  /**
   * Clear calculated input values cache
   */
  Compiler.prototype.clearCache = function () {
    this.inputsCache = {};
  };

  /**
   * Compile expression against scope.
   *
   * @param {string} expression
   * @param {jQuery} $scope
   * @returns {boolean}
   */
  Compiler.prototype.compile = function (expression, $scope) {
    var conditions = parser.parse(expression);

    if (!conditions) {
      return false;
    }

    var validatorBody = expression, expr;

    // evaluate each condition
    for (expr in conditions) {
      if (conditions.hasOwnProperty(expr)) {
        validatorBody = validatorBody.replace(
          this.getRegExp(expr),
          evalCondition(conditions[expr], $scope, this.inputsCache)
        );
      }
    }

    var result = false;
    try {
      result = (Function('return ' + validatorBody))();
    } catch (e) {
      console.error('ShowMeIf compiler: can\'t evaluate expression "%s". Error: %s.', validatorBody, e.message);
    }

    // running a unit test, no need for logs
    if (!isTestEnv && (console && console.group)) {
      var groupName = 'ShowMeIf compiler: ' + expression;
      console.group(groupName);
      console.log('compiled', validatorBody);
      console.log('evaluated', result);
      console.groupEnd(groupName);
    }

    return result;
  };

  return new Compiler();
})(jQuery);

module.exports = compiler;
