/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: : $
 * @changedby   : $
 */

var parser = require('../../src/parser');

describe('Parser', function () {
  describe('parse()', function() {
    it('should parse simple expression', function () {
      var parsed = parser.parse('cond1');
      parsed.should.be.eql({
        'cond1': {
          prop    : 'value',
          selector: ':input[name="cond1"]'
        }
      });
    });

    it('should parse complex expression', function () {
      var parsed = parser.parse('!((cond1:selected || !.cond2 || !numberInput) || input[name="radio"]:checked || [name="email"]:valid && (#input2:selected && .cond4.my:valid))');
      parsed.should.be.eql({
        'cond1:selected': {
          prop    : 'selected',
          selector: ':input[name="cond1"]'
        },
        '.cond2': {
          prop    : 'value',
          selector: '.cond2'
        },
        'numberInput': {
          prop    : 'value',
          selector: ':input[name="numberInput"]'
        },
        '[name="email"]:valid': {
          prop    : 'valid',
          selector: '[name="email"]'
        },
        'input[name="radio"]:checked': {
          prop    : 'checked',
          selector: 'input[name="radio"]'
        },
        '#input2:selected': {
          prop    : 'selected',
          selector: '#input2'
        },
        '.cond4.my:valid': {
          prop    : 'valid',
          selector: '.cond4.my'
        }
      });
    });
  });
});
