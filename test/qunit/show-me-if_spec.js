/**
 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
 * @version     SVN: : $
 * @changedby   : $
 */
(function ($) {
  'use strict';

  function getTplForm() {
    return [
      '<form>',
        '<div id="inputs">',
          '<input type="radio" name="radio1" value="0" id="radio1Field" class="radio1Field"/>',
          '<input type="radio" name="radio2" value="1" id="radio2Field" class="radio2Field"/>',
          '<input type="checkbox" name="checkbox1" value="0" id="checkbox1Field" class="checkbox1Field"/>',
          '<input type="checkbox" name="checkbox2" value="1" id="checkbox2Field" class="checkbox2Field"/>',
          '<select name="select" id="selectField" class="selectField">',
            '<option value="" selected disabled></option>',
            '<option value="1">1</option>',
            '<option value="2">2</option>',
            '<option value="3">3</option>',
            '<option value="4">4</option>',
          '</select>',
          '<input type="text" name="text" id="textField" class="textField"/>',
          '<input type="email" name="emailField" id="emailField" class="emailField"/>',
          '<input type="url" name="urlField" id="urlField" class="urlField"/>',
          '<input type="number" name="numberField" id="numberField" class="numberField"/>',
          '<input type="radio" name="sameNameField" id="sameNameField" class="sameNameField"/>',
          '<input type="radio" name="sameNameField" id="sameNameField1" class="sameNameField"/>',
        '</div>',
        '<div id="containers">',
          '<div id="radioListener" class="listener" data-if="radio1 || radio2">',
            '<input type="text"/>',
            '<button type="button"/>',
          '</div>',
          '<div id="checkbox1Listener" class="listener" data-if="checkbox1">',
            '<input type="text"/>',
            '<button type="button"/>',
          '</div>',
          '<div id="checkbox2Listener" class="listener" data-if="checkbox2:checked">',
            '<input type="text"/>',
            '<button type="button"/>',
          '</div>',
          '<div id="selectListener" class="listener" data-if="#selectField">',
            '<input type="text"/>',
            '<button type="button"/>',
          '</div>',
          '<div id="textListener" class="listener" data-if=".textField">',
            '<input type="text"/>',
            '<input type="button"/>',
          '</div>',
          '<div id="emailListener" class="listener" data-if="emailField && emailField:valid">',
            '<input type="text"/>',
            '<input type="button"/>',
          '</div>',
          '<div id="urlListener" class="listener" data-if="urlField && urlField:valid">',
            '<input type="text"/>',
            '<input type="button"/>',
          '</div>',
          '<div id="numberListener" class="listener" data-if="numberField && numberField:valid">',
            '<input type="text"/>',
            '<input type="button"/>',
          '</div>',
          '<div id="multipleFieldsListener" class="listener" data-if="radio1 && (numberField && numberField:valid) && (urlField && urlField:valid)">',
            '<input type="text"/>',
            '<input type="button"/>',
          '</div>',
          '<div id="sameNameFieldsListener" class="listener" data-if="sameNameField && sameNameField:valid">',
            '<input type="text"/>',
            '<input type="button"/>',
          '</div>',
        '</div>',
      '</form>'
    ].join('\n');
  }

  function childInputsDisabled($parents, disabled) {
    var expected = !!disabled,
      actual = expected;

    $parents.each(function () {
      $(':input:not(:button)', this).each(function () {
        if (!!this.disabled !== expected) {
          actual = false;
          return false;
        }
      });

      if (actual !== expected) {
        return false;
      }
    });

    return actual === expected;
  }

  function setInputsValue($parent, inputs, resetAll) {
    var $inputs = $(inputs || '#inputs :input', $parent);
    if (resetAll) {
      $inputs.val('').prop('checked', false);
    } else {
      $inputs.each(function () {
        var $el = $(this);
        if ($el.is(':radio') || $el.is(':checkbox')) {
          $el.val('1234').prop('checked', true);
        } else if ($el[0].tagName.toLowerCase() === 'select') {
          $el.find('option').not(':disabled').eq(0).prop('selected', true);
        } else if ($el[0].type === 'text') {
          $el.val('some value');
        } else if ($el[0].type === 'email') {
          $el.val('my@mail.com');
        } else if ($el[0].type === 'url') {
          $el.val('http://my-site.com');
        } else if ($el[0].type === 'number') {
          $el.val(123);
        }
      });
    }

    // trigger event listeners
    $inputs.change();
  }

  QUnit.module('ShowMeIf plugin', {
    beforeEach: function() {
      $('#qunit-fixture').html(getTplForm());
      this.elem = $('form', '#qunit-fixture');
      this.listeners = $('.listener', this.elem);
      this.elem.showMeIf();
    }
  });

  QUnit.test('should be initialized', function(assert) {
    var pluginData = this.elem.data('ShowMeIf');
    assert.ok(pluginData);
  });

  QUnit.test('should hide all listeners', function(assert) {
    var visible = this.listeners.filter(':visible');
    assert.ok(visible.length === 0);
    assert.ok(childInputsDisabled(this.listeners, true), 'child inputs disabled');
  });

  QUnit.test('should show only one matching listener', function(assert) {
    setInputsValue(this.elem, '#textField');
    var visible = this.listeners.filter(':visible');
    assert.ok(visible.length === 1);
    assert.ok(childInputsDisabled(visible, false), 'child inputs enabled');
  });

  QUnit.test('should show all listeners', function(assert) {
    setInputsValue(this.elem);
    var visible = this.listeners.filter(':visible');
    assert.ok(visible.length === this.listeners.length);
    assert.ok(childInputsDisabled(visible, false), 'child inputs enabled');
  });

  QUnit.test('should handle fields with same names', function(assert) {
    setInputsValue(this.elem, '.sameNameField:eq(0)');
    var visible = this.listeners.filter(':visible');
    assert.equal(visible.length, this.listeners.filter('#sameNameFieldsListener').length);
  });
})(jQuery);
