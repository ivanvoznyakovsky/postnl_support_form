/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * jQuery plugin to show/hide parts of the form based on an expression defined as "data-if" attribute.
	 * Expression is formed of the state(s) of any inputs of parent element (form).
	 * @see parser.js anf compiler.js for details.
	 *
	 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
	 */
	(function ($) {
	  'use strict';

	  var parser = __webpack_require__(1)
	    , compiler = __webpack_require__(2);

	  /**
	   * Plugin class definition.
	   *
	   * @param {jQuery} $el
	   * @constructor
	   */
	  var ShowMeIf = function ($el) {
	    this.$el = $el;
	    this.getEmitters();
	    this.watch();
	  };

	  /**
	   * Collect all input fields to listen their "change" event.
	   */
	  ShowMeIf.prototype.getEmitters = function () {
	    var _this = this,
	      selectors = _this.emitters = [];

	    _this.listeners = _this.$el.find('[data-if]').each(function () {
	      var $el = $(this),
	        expr = $el.data('if'),
	        parsed = parser.parse(expr);

	      if (parsed) {
	        // set listener's initial visibility
	        _this.toggle($el);

	        // collect selectors from parsed expression
	        $.each(parsed, function (k, v) {
	          selectors.indexOf(v.selector) === -1 && selectors.push(v.selector);
	        });
	      }
	    });
	  };

	  /**
	   * Listen for "change" event on emitters.
	   *
	   * @returns {boolean}
	   */
	  ShowMeIf.prototype.watch = function () {
	    var _this = this,
	      emitters = _this.emitters;

	    if (!emitters.length) {
	      return false;
	    }

	    // listen all possible emitters
	    // cause coupled radio buttons fire only one change event pr namespace
	    _this.$el.on('change', ':input:not(:button)', function () {
	      _this.listeners.each(function () {
	        _this.toggle(this);
	      });
	    });
	  };

	  /**
	   * Show/hide a container and disable/enable it child input fields
	   * based on evaluated expression if "data-if" attribute.
	   *
	   * @param {jQuery|HTMLElement} el
	   */
	  ShowMeIf.prototype.toggle = function (el) {
	    var $el = $(el),
	      show = compiler.compile($el.data('if'), this.$el);
	    $el[show ? 'fadeIn' : 'hide']()
	      .find(':input')
	      .add($el) // if it's an input then handle it to
	      .prop('disabled', !show);
	  };

	  /**
	   * Check if jQuery version valid.
	   * Plugin requires at least version 1.6 as we rely on prop()
	   *
	   * @returns {boolean}
	   */
	  var isValidLibVersion = function () {
	    var ver = $.fn.jquery.split('.');
	    return !(ver[0] === 0 || (ver[0] === 1 && ver[1] < 6));
	  };

	  /**
	   * Plugin definition.
	   *
	   * @returns {jQuery}
	   */
	  $.fn.showMeIf = function () {

	    if (!isValidLibVersion()) {
	      return console.error('ShowMeIf: jQuery is too old. Need at least v1.6.');
	    }

	    var componentName = 'ShowMeIf';
	    return this.each(function () {
	      var $el = $(this),
	        data = $el.data(componentName);
	      !data && $el.data(componentName, new ShowMeIf($el));
	    });
	  };

	  /**
	   * Automatically initialize plugin on marked elements.
	   */
	  $(function () {
	    $('.js-show-me-if').showMeIf();
	  });
	})(jQuery);





/***/ },
/* 1 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Helper class for parsing string expression into an object representation.
	 *
	 * Example: <pre><code>.inputCheckbox || #inputText && (inputEmail:valid && inputSelect)</code></pre>
	 * Each part of expression is a valid jQuery selector: <pre><code>.className/#inputId/inputName</code></pre>
	 * If a part of expression has no trailing "." or "#" character then it's assumed to be an input name.
	 * and will be interpreted as follows: <pre><code>:input[name='inputEmail']</code></pre>
	 *
	 * The following modifiers are supported: checked, selected, valid: <pre><code>inputEmail:valid</code></pre>
	 * Modifiers can not be combined in this version.
	 * Which means you can not do something like: <pre><code>inputEmail:valid:selected</code></pre>
	 *
	 * If no modifier given to a condition <pre><code>:checked, :selected, :valid</code></pre> then value of the input will be checked.
	 *
	 * Give example above will eventually be parsed as following set of conditions:
	 * <pre><code>
	 *     {
	 *        '.inputCheckbox': {
	 *          prop    : 'value',
	 *          selector: '.inputCheckbox'
	 *        },
	 *        '#inputText': {
	 *          prop    : 'value',
	 *          selector: '#inputText'
	 *        },
	 *        'inputEmail:valid': {
	 *          prop    : 'valid',
	 *          selector: ':input[name="inputEmail"]'
	 *        },
	 *        'inputSelect': {
	 *          prop    : 'value',
	 *          selector: ':input[name="inputSelect"]'
	 *        }
	 *     }
	 * </code></pre>
	 *
	 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
	 */

	'use strict';

	var parser = (function () {

	  __webpack_require__(3).string();

	  /**
	   * Parser class definition.
	   *
	   * @constructor
	   */
	  var Parser = function () {
	    this.cache = {};
	  };

	  /**
	   * Parse expression or get it from cache if exists.
	   *
	   * @param {string} expression
	   * @returns {object|false}
	   */
	  Parser.prototype.parse = function (expression) {
	    if (!expression) {
	      console.warn('ShowMeIf parser: expression is empty.');
	      return false;
	    }

	    var cache = this.cache;

	    if (!cache[expression]) {
	      // trim whitespaces
	      expression = expression
	        .trim()
	        .replace(/\s+/g, ' ') // two or more spaces to one
	        .replace(/\(|\)|!/g, ''); // remove brackets and negations

	      var parts = expression.split(/&&|\|\|/).map(function (el) {
	        return ('' + el).trim();
	      });

	      if (!parts.length) {
	        console.warn('ShowMeIf parser: invalid expression "%s".', expression);
	        return false;
	      }

	      var expressionFields = {},
	        fieldsEmpty = true,
	        i = parts.length,
	        part,
	        selector,
	        matches;

	      for (; i--;) {
	        part = parts[i];
	        matches = part.match(/^((?:\.|#)?(?:\[)?.+(?:\])?)/);

	        if (!matches || !matches[1]) {
	          continue;
	        }

	        selector = matches[1];
	        matches = selector.match(/(.+):(\w+)?$/);

	        if (matches) {
	          selector = matches[1];
	        }

	        // if it's not a class/id selector
	        // and not input name selector notation
	        if (!/\.|#|\[|\]/g.test(selector)) {
	          selector = ':input[name="' + selector + '"]'
	        }

	        var prop = (matches && matches[2]) || 'value';
	        expressionFields[part] = {
	          prop    : ('' + prop).toLowerCase().trim(),
	          selector: selector.trim()
	        };

	        fieldsEmpty = false;
	      }

	      if (!fieldsEmpty) {
	        cache[expression] = expressionFields;
	      }
	    }

	    return cache[expression];
	  };

	  return new Parser();
	})();

	module.exports = parser;


/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

	/**
	 * Helper class to compile conditions set into boolean result.
	 * NEVER GROUP CONTAINERS UNDER A COMMON CONDITION
	 * CAUSE IF CHILD CONTAINER'S CONDITION WILL EVALUATE TO "TRUE"
	 * IT'LL MAKE ALL INTERNAL INPUTS "ENABLED" EVEN IF PARENT'S CONTAINER CONDITION WILL BE "FALSE"
	 * Eg.:
	 * <pre>
	 *     <div data-if="commonCondition">
	 *         <div data-if="child1Condition">
	 *         <div data-if="child2Condition">
	 *         <div data-if="child3Condition">
	 *     </div>
	 * </pre>
	 *
	 * See parser.js for input format.
	 *
	 * @author      Ivan Voznyakovsky <ivan.voznyakovsky@gmail.com>
	 */

	'use strict';

	var compiler = (function ($) {

	  var parser = __webpack_require__(1);

	  /**
	   * @type {boolean}
	   */
	  var supportsNativeValidation = (function () {
	    return typeof document.createElement('input').checkValidity === 'function';
	  })();

	  /**
	   * @type {boolean}
	   */
	  var isTestEnv = /PhantomJS/.test(window.navigator.userAgent);

	  /**
	   * Evaluates set of boolean condition against given scope
	   *
	   * @param {object} condition
	   * @param {string} condition.prop
	   * @param {string} condition.selector
	   * @param {jQuery} $scope
	   * @returns {boolean}
	   */
	  var evalCondition = function (condition, $scope) {
	    if (!$scope || !$scope.length || $.isEmptyObject(condition)) {
	      return false;
	    }

	    var $el = $scope.find(condition.selector).filter(':input:not(:button)');

	    if (!$el.length) {
	      return false;
	    }

	    var propToCheck = condition.prop,
	      inputHasVal = !!$el.val();

	    if (!inputHasVal) {
	      return false;
	    }

	    var result = inputHasVal;
	    if (propToCheck === 'value') {
	      // if it's a radio button or a checkbox check it's state
	      if (['radio', 'checkbox'].indexOf($el[0].type) >= 0) {
	        result = inputHasVal && !$el.prop('disabled') && !!$el.filter(':checked').length;
	      }
	    } else if (propToCheck === 'valid') {
	      // check for valid/invalid classes first
	      if ($el.hasClass('valid') || $el.hasClass('invalid')) {
	        result = !$el.hasClass('invalid');
	      } else {
	        // if we can not check input's validity natively then assume it's valid
	        result = supportsNativeValidation ? $el[0].checkValidity() : true;
	      }
	    } else {
	      result = $el.prop(propToCheck);
	    }

	    return result;
	  };

	  /**
	   * Escape string for RegExp.
	   *
	   * @param {string} str
	   * @returns {string}
	   */
	  var escape = function (str) {
	    return ('' + str).replace(/(!|\.|\[|\])/g, '\\$1');
	  };

	  /**
	   * Compiler class definition.
	   *
	   * @constructor
	   */
	  var Compiler = function () {
	    this.reCache = {};
	    this.inputsCache = {};
	  };

	  /**
	   * Create a new RegExp object based on expression
	   * or get it from cache if exists.
	   *
	   * @param {string} expression
	   * @returns {RegExp}
	   */
	  Compiler.prototype.getRegExp = function (expression) {
	    var cache = this.reCache;
	    if (!cache[expression]) {
	      cache[expression] = new RegExp(escape(expression + '\\b'), 'g');
	    }

	    return cache[expression];
	  };

	  /**
	   * Clear calculated input values cache
	   */
	  Compiler.prototype.clearCache = function () {
	    this.inputsCache = {};
	  };

	  /**
	   * Compile expression against scope.
	   *
	   * @param {string} expression
	   * @param {jQuery} $scope
	   * @returns {boolean}
	   */
	  Compiler.prototype.compile = function (expression, $scope) {
	    var conditions = parser.parse(expression);

	    if (!conditions) {
	      return false;
	    }

	    var validatorBody = expression, expr;

	    // evaluate each condition
	    for (expr in conditions) {
	      if (conditions.hasOwnProperty(expr)) {
	        validatorBody = validatorBody.replace(
	          this.getRegExp(expr),
	          evalCondition(conditions[expr], $scope, this.inputsCache)
	        );
	      }
	    }

	    var result = false;
	    try {
	      result = (Function('return ' + validatorBody))();
	    } catch (e) {
	      console.error('ShowMeIf compiler: can\'t evaluate expression "%s". Error: %s.', validatorBody, e.message);
	    }

	    // running a unit test, no need for logs
	    if (!isTestEnv && (console && console.group)) {
	      var groupName = 'ShowMeIf compiler: ' + expression;
	      console.group(groupName);
	      console.log('compiled', validatorBody);
	      console.log('evaluated', result);
	      console.groupEnd(groupName);
	    }

	    return result;
	  };

	  return new Compiler();
	})(jQuery);

	module.exports = compiler;


/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

	'use strict';

	module.exports.string = function() {
	  if (!String.prototype.trim) {
	    (function () {
	      // Make sure we trim BOM and NBSP
	      var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;
	      String.prototype.trim = function () {
	        return this.replace(rtrim, '');
	      };
	    })();
	  }
	};


/***/ }
/******/ ]);