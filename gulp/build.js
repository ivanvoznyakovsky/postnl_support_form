'use strict';

var gulp = require('gulp');
var gulpWebpack = require('gulp-webpack');
var webpack = require('webpack');

module.exports = function() {
  gulp.task('build', ['clean:dist-js'], function () {
    return gulp.src('src/show-me-if.jquery.js')
      .pipe(gulpWebpack({
        output: {
          filename: 'show-me-if.jquery.js'
        }
      }, webpack))
      .pipe(gulp.dest('dist/'))
      .pipe(gulpWebpack({
        output: {
          filename: 'show-me-if.jquery.min.js'
        },
        plugins: [new webpack.optimize.UglifyJsPlugin()]
      }, webpack))
      .pipe(gulp.dest('dist/'));
  });
};
