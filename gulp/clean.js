'use strict';

var gulp = require('gulp');
var del = require('del');

module.exports = function () {
  gulp.task('clean:dist', function (done) {
    del('dist/*', done);
  });

  gulp.task('clean:dist-js', function (done) {
    del('dist/**/*.js', done);
  });

  gulp.task('clean:docs', function (done) {
    del('docs/*', done);
  });
};


