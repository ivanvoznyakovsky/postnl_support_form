'use strict';

var gulp = require('gulp'),
  qunit = require('node-qunit-phantomjs'),
  mocha = require('gulp-mocha');

module.exports = function () {
  gulp.task('test:qunit', function () {
    qunit('./test/qunit/index.html', {
      verbose: true
    });
  });

  gulp.task('test:mocha', function () {
    gulp.src('./test/spec/**/*.js', {read: false})
      .pipe(mocha({
        ui     : 'bdd',
        globals: {
          should: require('should')
        }
      })
      .once('error', function () {
          process.exit(1);
      })
      .once('end', function () {
          process.exit();
      }));
  });
};
