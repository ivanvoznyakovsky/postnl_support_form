'use strict';

var gulp = require('gulp');

module.exports = function() {
  gulp.task('watch', ['build'], function () {
    gulp.watch('src/**/*.js', function() {
      gulp.start('build', 'jsdoc');
    });

    gulp.watch('src/**/*.html', function() {
      gulp.start('fileinclude');
    });
  });
};
