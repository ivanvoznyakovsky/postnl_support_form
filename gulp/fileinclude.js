'use strict';

var fileinclude = require('gulp-file-include'),
  gulp = require('gulp');

module.exports = function () {
  gulp.task('fileinclude', function() {
    gulp.src(['src/index-local.html', 'src/index-deploy.html'])
      .pipe(fileinclude({
        prefix  : '@@',
        basepath: 'src'
      }))
      .pipe(gulp.dest('dist'));
  });
};

