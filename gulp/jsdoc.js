'use strict';

var gulp = require('gulp');
var doxx = require('gulp-doxx');

module.exports = function() {
  gulp.task('jsdoc', ['clean:docs'], function () {
    gulp.src("./src/**/*.js")
      .pipe(doxx())
      .pipe(gulp.dest('./docs'));
  });
};
